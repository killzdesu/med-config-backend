<?php
require 'vendor/autoload.php';
$f3 = \Base::instance();

// Use .env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Setup database
$db = new DB\SQL(
  'mysql:host='.$_ENV['DB_HOST'].';port=3306;dbname='.$_ENV['DB_NAME'],
  $_ENV['DB_USER'],
  $_ENV['DB_PASSWORD']
);
$f3->set('db', $db);

// Load all classes
$f3->set('AUTOLOAD','classes/');

// Activate CORS
$f3->set('CORS.origin', '*');
$f3->copy('HEADERS.Origin','CORS.origin');

$f3->route('GET /',
  function($f3) {
    // echo 'Hello, world!';
    $db = new Database;
    $chief = $db->getValue($f3, 'chiefmed', 'sheet_url');
    // var_dump($chief);
    // return $chief;
    echo json_encode($chief);
  }
);

$f3->route('GET /get-config',
  'Config->getConfig'
);

$f3->route('GET /get-config-all',
  'Config->getConfigAll'
);

$f3->route('GET /set-config',
  'Config->setConfig'
);

$f3->run();
