# Backend service for KKU

backend config for
- Chiefmed

## API

**ChiefMed**
View live [here](https://chiefmed-snh.onrender.com)

**REST API**
- Get config

`GET /get-config?table=***&name=***`

return json
`{
  "data": "***"
}`

or `GET /get-config-all?table=chiefmed`

return json
`{
  "sheet_url": "***",
  "sheet_range": "Code"
}`

- Edit config

with parameters

`GET /set-config?table=***&name=***&value=***`
