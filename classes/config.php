<?php
class Config {
  function getConfig($f3, $params) {
    $db = new Database;
    $table = $_GET['table'];
    $name = $_GET['name'];
    if($table == null || $name == null){
      echo json_encode(false);
      return;
    }
    $res = $db->getValue($f3, $table, $name);

    echo json_encode(array('data'=>$res), JSON_HEX_QUOT);
  }

  function getConfigAll($f3, $params) {
    $db = new Database;
    $table = $_GET['table'];
    if($table == null){
      echo json_encode(false);
      return;
    }
    $res = $db->getValueAll($f3, $table);

    echo json_encode($res, JSON_HEX_QUOT);
  }

  function setConfig($f3, $params) {
    $db = new Database;
    $table = $_GET['table'];
    $name = $_GET['name'];
    $value = $_GET['value'];
    if($table == null || $name == null || $value == null){
      echo json_encode(false);
      return;
    }
    $res = $db->setValue($f3, $table, $name, $value);

    echo json_encode(array("result"=>true), JSON_HEX_QUOT);

  }
}
