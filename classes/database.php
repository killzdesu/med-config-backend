<?php
class Database {
  function getValue($f3, $table, $name) {
    $res = $f3->get('db')->exec(
      'SELECT value FROM '.$table.' WHERE name=?',
      $name
    );
    return $res ? $res[0]['value'] : null;
  }

  function getValueAll($f3, $table) {
    $res = $f3->get('db')->exec(
      'SELECT name, value FROM '.$table
    );
    return $res;
  }

  function setValue($f3, $table, $name, $value) {
    $res = $f3->get('db')->exec(
      'UPDATE '.$table.' SET value=? WHERE name=?',
      array(0=>$value, 1=>$name)
    );
    // return $res ? $res[0]['value'] : null;
  }
}
